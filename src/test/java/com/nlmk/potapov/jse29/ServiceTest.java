package com.nlmk.potapov.jse29;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {
    Service service;

    @BeforeEach
    private void setup() {
        service = new Service();
    }


    @Test
    void sumCorrect() {
        long result = 5l;
        assertEquals(result, service.sum("2", "3"));
    }

    @Test
    void factorialCorrect() {
        long result = 120l;
        assertEquals(result, service.factorial("5"));
    }

    @Test
    void factorial0() {
        long result = 1l;
        assertEquals(result, service.factorial("0"));
    }

    @Test
    void factorialOverflow() {
        assertThrows(IllegalArgumentException.class, () -> service.factorial("1000"));
    }

    @Test
    void notPositive() {
        assertThrows(IllegalArgumentException.class, () -> service.factorial("-5"));
    }

    @Test
    void wrongArgument() {
        assertThrows(IllegalArgumentException.class, () -> service.factorial("stj"));
    }

    @Test
    void fibonacciCorrect() {
        long[] result = {0, 1, 1, 2, 3, 5, 8, 13, 21};
        assertArrayEquals(result, service.fibonacci("34"));
    }

    @Test
    void fibonacciError() {
        assertThrows(IllegalArgumentException.class, () -> service.fibonacci("17"));
    }
}