package com.nlmk.potapov.jse29;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Service {

    public long sum(String arg1, String arg2) {
        Long longArg1 = getLong(arg1);
        Long longArg2 = getLong(arg2);
        return longArg1 + longArg2;
    }

    public long factorial(String arg) {
        Long longArg = getLong(arg);
        checkPositive(longArg);
        Long result = 1L;
        while (longArg > 1) {
            try {
                result = Math.multiplyExact(result, longArg);
            } catch (ArithmeticException e) {
                throw new IllegalArgumentException();
            }
            longArg--;
        }
        return result;
    }

    public long[] fibonacci(String arg) {
        Long longArg = getLong(arg);
        checkPositive(longArg);
        List<Long> resultList = new ArrayList<>();
        resultList.add(0L);
        resultList.add(1L);
        int index = 1;
        while (longArg > resultList.get(index - 1) + resultList.get(index)) {
            resultList.add(resultList.get(index - 1) + resultList.get(index));
            index++;
        }
        if (!Objects.equals(longArg, resultList.get(index - 1) + resultList.get(index))) {
            throw new IllegalArgumentException();
        }
        long[] result = new long[resultList.size()];
        for (index = 0; index < resultList.size(); index++) {
            result[index] = resultList.get(index);
        }
        return result;
    }

    private long getLong(String arg) {
        try {
            return Long.parseLong(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
    }

    private void checkPositive(long arg) {
        if (arg < 0) {
            throw new IllegalArgumentException();
        }
    }

}
