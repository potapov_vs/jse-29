package com.nlmk.potapov.jse29;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Service service = new Service();
        Scanner scanner = new Scanner(System.in);


        String command = "help";
        while (!Objects.equals(command, "exit")) {
            switch (command) {
                case "sum": {
                    System.out.print("Введите первый аргумент: ");
                    String in1 = scanner.nextLine();
                    System.out.print("Введите второй фргумент: ");
                    String in2 = scanner.nextLine();
                    System.out.print("Результат расчета: ");
                    System.out.println(service.sum(in1, in2));
                    break;
                }
                case "factorial": {
                    System.out.print("Введите аргумент: ");
                    String in = scanner.nextLine();
                    System.out.print("Результат расчета: ");
                    System.out.println(service.factorial(in));
                    break;
                }
                case "fibonacci": {
                    System.out.print("Введите аргумент: ");
                    String in = scanner.nextLine();
                    System.out.print("Результат расчета: ");
                    System.out.println(Arrays.toString(service.fibonacci(in)));
                    break;
                }
                case "help": {
                    System.out.println("Список команд: ");
                    System.out.println("    sum - Вычисление суммы");
                    System.out.println("    factorial - Вычисление факториала");
                    System.out.println("    fibonacci - Разложение на сумму чисел Фибоначчи");
                    System.out.println("    help - Помощь");
                    System.out.println("    exit - Выход");
                    break;
                }
                default: {
                    System.out.println("Список команд: ");
                    System.out.println("    sum - Вычисление суммы");
                    System.out.println("    factorial - Вычисление факториала");
                    System.out.println("    fibonacci - Разложение на сумму чисел Фибоначчи");
                    System.out.println("    help - Помощь");
                    System.out.println("    exit - Выход");
                }
            }
            System.out.print("Введите команду: ");
            command = scanner.nextLine();
        }
    }
}
